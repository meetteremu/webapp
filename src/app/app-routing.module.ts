import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './auth.guard';
import { HomepageComponent } from './components/homepage/homepage.component';
import { AdminAreaComponent } from './layouts/admin-area/admin-area.component';
import { AdminHomepageComponent } from './components/admin-homepage/admin-homepage.component';
import { AdminAddCategoryComponent } from './components/admin-add-category/admin-add-category.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  
  // admin
  { path: 'admin', redirectTo: 'admin/homepage', pathMatch: 'full' },
  { path: 'admin', component: AdminAreaComponent,  canActivate: [AuthGuard], 
    children: [
        { path: 'homepage', component: AdminHomepageComponent },
        { path: 'add-category', component: AdminAddCategoryComponent },
    ]
  },
  
  // user normal
  { path: 'homepage', component: HomepageComponent, canActivate: [AuthGuard] },
  
  // redirections
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
