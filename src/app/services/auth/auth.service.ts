import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';


@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private readonly notifier: NotifierService;

  userStored;
  userTypeId;

  constructor(
    private api: ApiService,
    private router: Router,
    private notifierService: NotifierService) {
      this.notifier = notifierService;
  }


  login(data): void {
    this.api.post('login', data).subscribe(
      data => {
          // console.log("data login", data);
          localStorage.setItem('user', JSON.stringify(data.user));
          localStorage.setItem('token', data.token);
          this.api.setHeaderOptions();
          this.userStored = JSON.parse(localStorage.getItem('user'));
          this.userTypeId = this.userStored['user_type_id'];

          if (this.userTypeId === 1) {
            this.router.navigate(["/admin/homepage"]);
          } else if (this.userTypeId === 2) {
            this.router.navigate(["/homepage"]);
          } else {
            alert('Error userTypeId');
          }


        }
      ,
      err => {
        //alert("Il semblerait qu'il y ait un souci. Veuillez réessayer.")
        this.notifier.notify( "error", "Il semblerait qu'il y ait un souci. Veuillez réessayer." );
      }
    );
  }

  isLogged() {
    if(localStorage.getItem('token')) {
      return true;
    }
    return false;
  }

  logout() {
    this.api.get('logout').subscribe(data => {
      localStorage.clear();
      this.router.navigate(['/login']);
    });
  }

}
