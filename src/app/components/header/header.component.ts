import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  username;
  userStored;

  constructor(private auth: AuthService) { }

  ngOnInit(): void {
    this.userStored = JSON.parse(localStorage.getItem('user'));
    this.username = this.userStored['username'];
  }


  logout() {
    this.auth.logout();
  }

}
