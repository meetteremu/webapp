import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-admin-homepage',
  templateUrl: './admin-homepage.component.html',
  styleUrls: ['./admin-homepage.component.scss']
})
export class AdminHomepageComponent implements OnInit {
  userStored;
  username;
  meetings;
  categories;
  userId;

  constructor(
    private api: ApiService, 
    private router: Router, 
    private formBuilder: FormBuilder,
    private auth: AuthService
  ) { }

  ngOnInit(): void {
    this.userStored = JSON.parse(localStorage.getItem('user'));
    this.username = this.userStored['username'];
    this.userId = this.userStored['id'];
    this.getMeetings();
  }


  getMeetings() {
    this.api.get('meetings').subscribe(data => {
      localStorage.setItem('meetings', JSON.stringify(data));
      this.meetings = JSON.parse(localStorage.getItem('meetings'));
    })
  }

  getCategories () {
    this.api.get('categories').subscribe(data => {
      localStorage.setItem('categories', JSON.stringify(data));
      this.categories = JSON.parse(localStorage.getItem('categories'));
    })
  }

  onDeleteMeeting(id) {
    this.api.delete('meetings/delete/' + id).subscribe(data => {
      this.getMeetings();
    })
  }

}
