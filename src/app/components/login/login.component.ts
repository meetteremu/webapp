import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm;
  userStored;
  userTypeId;

  constructor(
    private api: ApiService, 
    private router: Router, 
    private formBuilder: FormBuilder,
    private auth: AuthService
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.userStored = JSON.parse(localStorage.getItem('user'));
    if(this.userStored) {
      this.userTypeId = this.userStored['user_type_id'];
    }

    if(this.userTypeId) {
      if (this.userTypeId === 1) {
        this.router.navigate(["/admin/homepage"]);
      } else if (this.userTypeId === 2) {
        this.router.navigate(["/homepage"]);
      } else {
        alert('Error userTypeId');
      }
    }
  }

  initForm() {
    this.loginForm = this.formBuilder.group({
      username: ['',Validators.required],
      password: ['',Validators.required],
    })
  }

  onSubmit() {
    this.auth.login(this.loginForm.value);
  }

}
