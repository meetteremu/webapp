import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-admin-add-category',
  templateUrl: './admin-add-category.component.html',
  styleUrls: ['./admin-add-category.component.scss']
})
export class AdminAddCategoryComponent implements OnInit {
  userStored;
  username;
  categories;
  userId;
  categoryForm

  constructor(
    private api: ApiService, 
    private router: Router, 
    private formBuilder: FormBuilder,
    private auth: AuthService
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.getCategories();
  }

  initForm() {
    this.categoryForm = this.formBuilder.group({
      name: ['', Validators.required]
    })
  }

  getCategories () {
    this.api.get('categories').subscribe(data => {
      localStorage.setItem('categories', JSON.stringify(data));
      this.categories = JSON.parse(localStorage.getItem('categories'));
    })
  }

  onSubmit() {
    this.api.post('categories/create', this.categoryForm.value).subscribe(data => {
      this.initForm();
      this.getCategories();
    })
  }

  onDelete(id) {
    this.api.delete('categories/delete/' + id).subscribe(data => {
      this.getCategories();
    });
  }
}
