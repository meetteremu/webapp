import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  userStored;
  username;
  meetings;
  categories;
  meetingForm;
  userId;

  constructor(
    private api: ApiService, 
    private router: Router, 
    private formBuilder: FormBuilder,
    private auth: AuthService
  ) { }

  ngOnInit(): void {
    this.userStored = JSON.parse(localStorage.getItem('user'));
    this.username = this.userStored['username'];
    this.userId = this.userStored['id'];
    
    this.initform();
    this.getMeetings();
    this.getCategories();

  }

  initform() {
    this.meetingForm = this.formBuilder.group({
      categoryId: ['', Validators.required],
      duration: ['', Validators.required],
      userId: this.userId,
    })
  }

  getMeetings() {
    this.api.get('meetings').subscribe(data => {
      localStorage.setItem('meetings', JSON.stringify(data));
      this.meetings = JSON.parse(localStorage.getItem('meetings'));
    })
  }

  getCategories () {
    this.api.get('categories').subscribe(data => {
      localStorage.setItem('categories', JSON.stringify(data));
      this.categories = JSON.parse(localStorage.getItem('categories'));
    })
  }

  onSubmit() {
    this.api.post('meetings/create', this.meetingForm.value).subscribe(data => {
      this.initform();
      this.getMeetings();
    });
  }

  logout() {
    this.auth.logout();
  }



}
